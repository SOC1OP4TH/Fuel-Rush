using System;
using System.Threading;
//using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FuelManager : MonoBehaviour
{

    public Image fuelBar;
   float fuel;
   Ship ship;
   bool isInBase = false;
   Rigidbody2D shipRB;
    public float fuelReduceRate = 5;
    void Start()
    {
    ship = GetComponent<Ship>();
    shipRB =  GetComponent<Rigidbody2D>();
    Debug.Log($"sa");
    }
   void  Update()
   {
    fuel =  ship.GetFuel();
   // Debug.Log($"Fuel={fuel}");
  
    if(!isInBase)
    ship.ReduceFuel(fuelReduceRate*shipRB.velocity.magnitude*Time.deltaTime);
    //Debug.Log(shipRB.velocity.magnitude);
    CheckGas();
    //if key A pressed
     FillFuelBar();
   }
   void FillFuelBar()
   {
    float fuelBarValue = fuel/100f;
    fuelBar.fillAmount = fuelBarValue;
    fuelBar.color = Color.Lerp(Color.red,Color.green,fuelBarValue);
   }
  
   void OnTriggerStay2D(Collider2D other)
   {
        if(other.gameObject.tag == "base")
        {
            ship.AddFuel(Time.deltaTime*25);
            isInBase = true;
        }
   }
   void OnTriggerExit2D(Collider2D other)
   {
        if(other.gameObject.tag == "base")
            isInBase = false;
   }
   void CheckGas()
   {
        if(MathF.Max(fuel,0) == 0)
            GameManager.Instance.gameOver = true;
   }
}
