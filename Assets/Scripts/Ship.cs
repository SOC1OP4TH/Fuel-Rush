using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
   [SerializeField] private float _fuel = 100;
   [SerializeField] private float _maxFuel = 100;
   [SerializeField] private Rigidbody2D _rb;

  /// <summary>
   /// Start is called on the frame when a script is enabled just before
   /// any of the Update methods is called the first time.
   /// </summary>
   private void Start()
   {
      _rb = GetComponent<Rigidbody2D>();
   }
   public Rigidbody2D GetRigidbody()
   {
      return _rb;
   }
   public float GetFuel()
   {
    return _fuel;
   }
   public void AddFuel(float value)
   {
    _fuel = MathF.Min(_fuel+value,100);
   }
   public void ReduceFuel(float value)
   {
      _fuel = MathF.Max(_fuel-value,0); 
   }

}
