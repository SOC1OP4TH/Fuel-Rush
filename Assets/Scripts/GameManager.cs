using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    float score;
    public float scoreRate;
    public static GameManager Instance => instance;
    public bool gameOver = false;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if (!gameOver)
            score += scoreRate * Time.deltaTime;
    }
}