using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float _speed = 15; 
    [SerializeField] private Rigidbody2D _rb;
    [SerializeField] private FloatingJoystick _joyStick;
    // Update is called once per frame
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        Move();
    }
    void Move()
    {
        Vector3 movePos = new Vector3(_joyStick.Horizontal,_joyStick.Vertical);
        _rb.velocity = movePos*_speed; 
       // Debug.Log($"horizontal={_joyStick.Horizontal},vertical is {_joyStick.Vertical}, velocity is {_rb.velocity}"); 
        //look where I turn
        if (_rb.velocity.magnitude > 0)
            transform.rotation = Quaternion.LookRotation(Vector3.forward, _rb.velocity);
        
    }
}
