
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



public class Arrow : MonoBehaviour
{   
   
    public List<GameObject> Bases;
   // public 
    GameObject nearestBase;
    float nearestBaseDistance = Mathf.Infinity;
    float baseTriggerDistance = 0.5f;
    
    // Start is called before the first frame update
    void Start()
    {
     Bases = new List<GameObject>(GameObject.FindGameObjectsWithTag("base"));
     UpdateArrowRotation();
    }

    // Update is called once per frame
    void Update()
    {
       
       FindNearestBase();
       UpdateArrowRotation();
    }
  void FindNearestBase()
{
    if (nearestBase != null)
    {
        float distanceToNearestBase = Vector2.Distance(transform.position, nearestBase.transform.position);
        if (distanceToNearestBase > nearestBaseDistance)
        {
            nearestBase = null;
            nearestBaseDistance = Mathf.Infinity;
        }
    }

    foreach (var item in Bases)
    {
        float distance = Vector2.Distance(transform.position, item.transform.position);
        if (distance < nearestBaseDistance)
        {
            nearestBase = item;
            nearestBaseDistance = distance;
        }
    }

   
}
    void UpdateArrowRotation()
{
    if (nearestBase != null)
    {
        Vector3 directionToNearestBase = nearestBase.transform.position - transform.position;
        float angle = Mathf.Atan2(directionToNearestBase.y, directionToNearestBase.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward) * Quaternion.Euler(0, 0, 180);
      //  Debug.Log($"Nearest base is {nearestBase}. {nearestBaseDistance} far");
    }
    else
    Debug.Log($"No base found");    
}


}
