﻿#include "pch-c.h"


#include "codegen/il2cpp-codegen-metadata.h"





extern void JoystickPlayerExample_FixedUpdate_m9AEDBA111F95D67A006A5D3821956048224541B7 (void);
extern void JoystickPlayerExample__ctor_m702422E0AE29402330CF41FDDBEE76F0506342E2 (void);
extern void JoystickSetterExample_ModeChanged_m35AF30EE3E6C8CEBF064A7AB80F5795E9AF06D23 (void);
extern void JoystickSetterExample_AxisChanged_m5CA220FEA14E06BD8A445E31C5B66E4601C5E404 (void);
extern void JoystickSetterExample_SnapX_m25A77C3DE4C6DBBE3B4A58E2DE8CD44B1773D6A1 (void);
extern void JoystickSetterExample_SnapY_m54FE8DCB2CE8D2BF5D2CDF84C68DE263F0E25B1B (void);
extern void JoystickSetterExample_Update_m99B2432D22FE669B4DC3209696AD4A62096C7D41 (void);
extern void JoystickSetterExample__ctor_m2A3D66E05BCDF78D0F116348094717BEBA73EC91 (void);
extern void Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA (void);
extern void Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE (void);
extern void Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC (void);
extern void Joystick_get_HandleRange_mB38F0D3B6ADE2D1557D7A3D6759C469F17509D77 (void);
extern void Joystick_set_HandleRange_m686B579A1F02EFCD4878BEA27EA606FC23CD2505 (void);
extern void Joystick_get_DeadZone_mCE52B635A8CF24F6DD867C14E34094515DE6AEFC (void);
extern void Joystick_set_DeadZone_mD5699A14E5284026F303C8AF8D3457DFA9920F19 (void);
extern void Joystick_get_AxisOptions_mA74F5FEE31C158E5179F0B108272ED28A661E388 (void);
extern void Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6 (void);
extern void Joystick_get_SnapX_m51CAFDCC399606BA82986908700AAA45668A0F40 (void);
extern void Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A (void);
extern void Joystick_get_SnapY_m35AFC1AD1DF5EDE5FCE8BAFEBE91AD51D7451613 (void);
extern void Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86 (void);
extern void Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C (void);
extern void Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6 (void);
extern void Joystick_OnDrag_m39E69636AEDC0E471EAD1371A99F4694ECDBA1E9 (void);
extern void Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C (void);
extern void Joystick_FormatInput_mDDF7AF40138CF227F0106811C8749180FBF45342 (void);
extern void Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987 (void);
extern void Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C (void);
extern void Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024 (void);
extern void Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B (void);
extern void DynamicJoystick_get_MoveThreshold_m16C670C1DA0A45E83F6F87C4304F459EDDEEDD5A (void);
extern void DynamicJoystick_set_MoveThreshold_m000C1AE325C0B9C33172E4202F2AFB59820517F9 (void);
extern void DynamicJoystick_Start_mFE16C6CE0B753F08E79A2AEC75782DEEF3B96F72 (void);
extern void DynamicJoystick_OnPointerDown_mBFA3026A0DA4A6B53C0E747A1E892CBC7F43E136 (void);
extern void DynamicJoystick_OnPointerUp_m10389907A9D3362F6B75FDC5F35AF37A5DD5AE7C (void);
extern void DynamicJoystick_HandleInput_m3F157F4825BE6682228C8E135581C6404D268506 (void);
extern void DynamicJoystick__ctor_m9DDA6263314BD7B97679DF14A4664358BD3E58CB (void);
extern void FixedJoystick__ctor_m8C8BB74E5EA8CA2C3DD2AE084301EC91F519AD24 (void);
extern void FloatingJoystick_Start_mB22059CD82AF77A8F94AC72E81A8BAE969399E81 (void);
extern void FloatingJoystick_OnPointerDown_mFE5B4F54D5BBCA72F9729AB64765F558FA5C7A54 (void);
extern void FloatingJoystick_OnPointerUp_m80ABA9C914E1953F91DBF74853CE84879352280D (void);
extern void FloatingJoystick__ctor_m6B72425996D46B025F9E9D22121E9D01BEC6BD3C (void);
extern void VariableJoystick_get_MoveThreshold_m8C9D3A63DB3B6CF1F0139C3504EC2E7AC4E7CF99 (void);
extern void VariableJoystick_set_MoveThreshold_m23DC4187B405EB690D297379E2113568B40C705A (void);
extern void VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE (void);
extern void VariableJoystick_Start_m21743760641EA0317ACCD95949B99825446FE74D (void);
extern void VariableJoystick_OnPointerDown_m8ABE5C8EFBC8DB3A2EE135FFF3C0F67C533AF4B5 (void);
extern void VariableJoystick_OnPointerUp_m65296D82A6C2E1BDC2D622B9C922FAE8E4544526 (void);
extern void VariableJoystick_HandleInput_mD1BCF9A068737A9C057EE8CEB7E6DEB682CC03AB (void);
extern void VariableJoystick__ctor_m6C7B41973BE20A94F16DB5DCC9AA805C5D8DF852 (void);
extern void Arrow_Start_m846685171DBFA6340C8E7614D77ABAAD1D1B50C1 (void);
extern void Arrow_Update_mCD196E06CFB34D01F63A834F64D65615E65E48DF (void);
extern void Arrow_FindNearestBase_m4C1592D80329AA978970B3A46205996BB852EB1F (void);
extern void Arrow_UpdateArrowRotation_m9367AB349A33B10CF48E26BB6A17CF6C0E168BDC (void);
extern void Arrow__ctor_m94A543A34B422016042ADD49083D7261EB6B2F80 (void);
extern void CameraFollow_Update_mFE010EED058E2CE97530268AC64A2C071D5F853A (void);
extern void CameraFollow__ctor_m113CC547D419EF599BC487F0F44B06BB2D4EE11B (void);
extern void FuelManager_Start_m05C3F5932F09BDB72A54712CF765DB2186B4CFED (void);
extern void FuelManager_Update_m08A5D89EC472483C6D216C60E102CD7769867534 (void);
extern void FuelManager_Fill_mB3195050D04B43BDF1CB1C23D0273DA5309D28B1 (void);
extern void FuelManager_OnTriggerStay2D_m4F485DB9EDF7226437C3D866587FADD57BDD0110 (void);
extern void FuelManager_OnTriggerExit2D_m6C7241693C6803773CDFFC1BF010430E663A4F3C (void);
extern void FuelManager__ctor_m736D089BA732BF278644A094A1F0CD3D43850B48 (void);
extern void GameManager_get_Instance_m076FE4D98E785B5AEE0B4C360C7857F824E7FBD0 (void);
extern void GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2 (void);
extern void GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41 (void);
extern void GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368 (void);
extern void Movement_Start_m3D09153FD03F06C31BBD151C21BA361EA49FA72B (void);
extern void Movement_Update_m4B99F519DF0A29B476F90FE4314A770CD53EC418 (void);
extern void Movement_Move_m67408F19FB9F9E0D97D765ACE89D531CC69718DE (void);
extern void Movement__ctor_mCB72C1AD57256D73959D74FB86C5D0AA69EAE7ED (void);
extern void Ship_GetFuel_m50100220DD8B74F822DE162E1F019F28BE21A2AD (void);
extern void Ship_AddFuel_mFACF136472FC07792FA5B23B928EEB1253C54D94 (void);
extern void Ship_ReduceFuel_m569BBA9F20E4DD36E5AB8F444FE598E3BFF69CE6 (void);
extern void Ship__ctor_m8D6A536949CE20E5E29BE93FF78A7D948E69A841 (void);
extern void UI_Manager__ctor_m73CE09593602D858992558F94668FFD0AEEF2FFB (void);
extern void UnitySourceGeneratedAssemblyMonoScriptTypes_v1_Get_mBEB95BEB954BB63E9710BBC7AD5E78C4CB0A0033 (void);
extern void UnitySourceGeneratedAssemblyMonoScriptTypes_v1__ctor_mE70FB23ACC1EA12ABC948AA22C2E78B2D0AA39B1 (void);
static Il2CppMethodPointer s_methodPointers[78] = 
{
	JoystickPlayerExample_FixedUpdate_m9AEDBA111F95D67A006A5D3821956048224541B7,
	JoystickPlayerExample__ctor_m702422E0AE29402330CF41FDDBEE76F0506342E2,
	JoystickSetterExample_ModeChanged_m35AF30EE3E6C8CEBF064A7AB80F5795E9AF06D23,
	JoystickSetterExample_AxisChanged_m5CA220FEA14E06BD8A445E31C5B66E4601C5E404,
	JoystickSetterExample_SnapX_m25A77C3DE4C6DBBE3B4A58E2DE8CD44B1773D6A1,
	JoystickSetterExample_SnapY_m54FE8DCB2CE8D2BF5D2CDF84C68DE263F0E25B1B,
	JoystickSetterExample_Update_m99B2432D22FE669B4DC3209696AD4A62096C7D41,
	JoystickSetterExample__ctor_m2A3D66E05BCDF78D0F116348094717BEBA73EC91,
	Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA,
	Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE,
	Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC,
	Joystick_get_HandleRange_mB38F0D3B6ADE2D1557D7A3D6759C469F17509D77,
	Joystick_set_HandleRange_m686B579A1F02EFCD4878BEA27EA606FC23CD2505,
	Joystick_get_DeadZone_mCE52B635A8CF24F6DD867C14E34094515DE6AEFC,
	Joystick_set_DeadZone_mD5699A14E5284026F303C8AF8D3457DFA9920F19,
	Joystick_get_AxisOptions_mA74F5FEE31C158E5179F0B108272ED28A661E388,
	Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6,
	Joystick_get_SnapX_m51CAFDCC399606BA82986908700AAA45668A0F40,
	Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A,
	Joystick_get_SnapY_m35AFC1AD1DF5EDE5FCE8BAFEBE91AD51D7451613,
	Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86,
	Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C,
	Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6,
	Joystick_OnDrag_m39E69636AEDC0E471EAD1371A99F4694ECDBA1E9,
	Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C,
	Joystick_FormatInput_mDDF7AF40138CF227F0106811C8749180FBF45342,
	Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987,
	Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C,
	Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024,
	Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B,
	DynamicJoystick_get_MoveThreshold_m16C670C1DA0A45E83F6F87C4304F459EDDEEDD5A,
	DynamicJoystick_set_MoveThreshold_m000C1AE325C0B9C33172E4202F2AFB59820517F9,
	DynamicJoystick_Start_mFE16C6CE0B753F08E79A2AEC75782DEEF3B96F72,
	DynamicJoystick_OnPointerDown_mBFA3026A0DA4A6B53C0E747A1E892CBC7F43E136,
	DynamicJoystick_OnPointerUp_m10389907A9D3362F6B75FDC5F35AF37A5DD5AE7C,
	DynamicJoystick_HandleInput_m3F157F4825BE6682228C8E135581C6404D268506,
	DynamicJoystick__ctor_m9DDA6263314BD7B97679DF14A4664358BD3E58CB,
	FixedJoystick__ctor_m8C8BB74E5EA8CA2C3DD2AE084301EC91F519AD24,
	FloatingJoystick_Start_mB22059CD82AF77A8F94AC72E81A8BAE969399E81,
	FloatingJoystick_OnPointerDown_mFE5B4F54D5BBCA72F9729AB64765F558FA5C7A54,
	FloatingJoystick_OnPointerUp_m80ABA9C914E1953F91DBF74853CE84879352280D,
	FloatingJoystick__ctor_m6B72425996D46B025F9E9D22121E9D01BEC6BD3C,
	VariableJoystick_get_MoveThreshold_m8C9D3A63DB3B6CF1F0139C3504EC2E7AC4E7CF99,
	VariableJoystick_set_MoveThreshold_m23DC4187B405EB690D297379E2113568B40C705A,
	VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE,
	VariableJoystick_Start_m21743760641EA0317ACCD95949B99825446FE74D,
	VariableJoystick_OnPointerDown_m8ABE5C8EFBC8DB3A2EE135FFF3C0F67C533AF4B5,
	VariableJoystick_OnPointerUp_m65296D82A6C2E1BDC2D622B9C922FAE8E4544526,
	VariableJoystick_HandleInput_mD1BCF9A068737A9C057EE8CEB7E6DEB682CC03AB,
	VariableJoystick__ctor_m6C7B41973BE20A94F16DB5DCC9AA805C5D8DF852,
	Arrow_Start_m846685171DBFA6340C8E7614D77ABAAD1D1B50C1,
	Arrow_Update_mCD196E06CFB34D01F63A834F64D65615E65E48DF,
	Arrow_FindNearestBase_m4C1592D80329AA978970B3A46205996BB852EB1F,
	Arrow_UpdateArrowRotation_m9367AB349A33B10CF48E26BB6A17CF6C0E168BDC,
	Arrow__ctor_m94A543A34B422016042ADD49083D7261EB6B2F80,
	CameraFollow_Update_mFE010EED058E2CE97530268AC64A2C071D5F853A,
	CameraFollow__ctor_m113CC547D419EF599BC487F0F44B06BB2D4EE11B,
	FuelManager_Start_m05C3F5932F09BDB72A54712CF765DB2186B4CFED,
	FuelManager_Update_m08A5D89EC472483C6D216C60E102CD7769867534,
	FuelManager_Fill_mB3195050D04B43BDF1CB1C23D0273DA5309D28B1,
	FuelManager_OnTriggerStay2D_m4F485DB9EDF7226437C3D866587FADD57BDD0110,
	FuelManager_OnTriggerExit2D_m6C7241693C6803773CDFFC1BF010430E663A4F3C,
	FuelManager__ctor_m736D089BA732BF278644A094A1F0CD3D43850B48,
	GameManager_get_Instance_m076FE4D98E785B5AEE0B4C360C7857F824E7FBD0,
	GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2,
	GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41,
	GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368,
	Movement_Start_m3D09153FD03F06C31BBD151C21BA361EA49FA72B,
	Movement_Update_m4B99F519DF0A29B476F90FE4314A770CD53EC418,
	Movement_Move_m67408F19FB9F9E0D97D765ACE89D531CC69718DE,
	Movement__ctor_mCB72C1AD57256D73959D74FB86C5D0AA69EAE7ED,
	Ship_GetFuel_m50100220DD8B74F822DE162E1F019F28BE21A2AD,
	Ship_AddFuel_mFACF136472FC07792FA5B23B928EEB1253C54D94,
	Ship_ReduceFuel_m569BBA9F20E4DD36E5AB8F444FE598E3BFF69CE6,
	Ship__ctor_m8D6A536949CE20E5E29BE93FF78A7D948E69A841,
	UI_Manager__ctor_m73CE09593602D858992558F94668FFD0AEEF2FFB,
	UnitySourceGeneratedAssemblyMonoScriptTypes_v1_Get_mBEB95BEB954BB63E9710BBC7AD5E78C4CB0A0033,
	UnitySourceGeneratedAssemblyMonoScriptTypes_v1__ctor_mE70FB23ACC1EA12ABC948AA22C2E78B2D0AA39B1,
};
static const int32_t s_InvokerIndices[78] = 
{
	4837,
	4837,
	3855,
	3855,
	3805,
	3805,
	4837,
	4837,
	4778,
	4778,
	4827,
	4778,
	3919,
	4778,
	3919,
	4703,
	3855,
	4654,
	3805,
	4654,
	3805,
	4837,
	3885,
	3885,
	724,
	4837,
	1588,
	3885,
	3547,
	4837,
	4778,
	3919,
	4837,
	3885,
	3885,
	724,
	4837,
	4837,
	4837,
	3885,
	3885,
	4837,
	4778,
	3919,
	3855,
	4837,
	3885,
	3885,
	724,
	4837,
	4837,
	4837,
	4837,
	4837,
	4837,
	4837,
	4837,
	4837,
	4837,
	4837,
	3885,
	3885,
	4837,
	7302,
	4837,
	4837,
	4837,
	4837,
	4837,
	4837,
	4837,
	4778,
	3919,
	3919,
	4837,
	4837,
	7354,
	4837,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	78,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
